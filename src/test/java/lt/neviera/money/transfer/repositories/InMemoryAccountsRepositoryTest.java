package lt.neviera.money.transfer.repositories;

import lt.neviera.money.transfer.domain.models.Account;
import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
class InMemoryAccountsRepositoryTest {

    @Test
    void should_save_and_find_created_account() {
        var sut = new InMemoryAccountsRepository();
        var account = Account.create("email");
        sut.save(account);
        assertEquals(1, sut.findAll().size());
        assertTrue(sut.find(account.id).isPresent());
    }

    @Test
    void should_delete_saved_account() {
        var sut = new InMemoryAccountsRepository();
        var account = Account.create("email");
        sut.save(account);
        assertEquals(1, sut.findAll().size());
        assertTrue(sut.find(account.id).isPresent());
        sut.delete(account.id);
        assertEquals(0, sut.findAll().size());
        assertTrue(sut.find(account.id).isEmpty());
    }
}