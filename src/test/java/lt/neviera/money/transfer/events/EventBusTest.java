package lt.neviera.money.transfer.events;

import lt.neviera.money.transfer.events.models.*;
import org.junit.jupiter.api.*;

@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
class EventBusTest {

    @Test
    void should_observe_only_specific_event_type() {
        var sut = new EventBus();
        var testSubscriber = sut.observeEvents(TestEventA.class).test();
        sut.publish(new TestEventB());
        sut.publish(new TestEventA());

        testSubscriber.assertNoErrors().assertValueCount(1);
    }

    @Test
    void should_observe_only_specific_command_type() {
        var sut = new EventBus();
        var testSubscriber = sut.observeCommands(TestCommandA.class).test();
        sut.publish(new TestCommandB());
        sut.publish(new TestCommandA());

        testSubscriber.assertNoErrors().assertValueCount(1);
    }

    private static class TestEventA implements Event {
    }

    private static class TestEventB implements Event {
    }

    private static class TestCommandA implements Command {
    }

    private static class TestCommandB implements Command {
    }
}