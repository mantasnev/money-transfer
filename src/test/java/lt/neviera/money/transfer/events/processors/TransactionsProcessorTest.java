package lt.neviera.money.transfer.events.processors;

import io.reactivex.disposables.Disposable;
import lt.neviera.money.transfer.domain.models.Account;
import lt.neviera.money.transfer.events.EventBus;
import lt.neviera.money.transfer.events.models.Event;
import lt.neviera.money.transfer.events.models.commands.*;
import lt.neviera.money.transfer.events.models.events.*;
import lt.neviera.money.transfer.repositories.*;
import org.junit.jupiter.api.*;

import java.math.BigDecimal;
import java.util.UUID;

@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
class TransactionsProcessorTest {

    private EventBus eventBus;
    private AccountsRepository accountsRepository;
    private Disposable disposable;

    @BeforeEach
    void setUp() {
        eventBus = new EventBus();
        accountsRepository = new InMemoryAccountsRepository();
        var transactionsProcessor = new TransactionsProcessor(accountsRepository);
        disposable = transactionsProcessor.subscribe(eventBus);
    }

    @AfterEach
    void tearDown() {
        disposable.dispose();
        eventBus.shutdown();
    }

    @Test
    void should_push_credited_successfully_event_on_top_up() {
        var eventTestSubscriber = eventBus.observeEvents(Event.class).test();

        var account = accountsRepository.save(Account.create("email"));
        var transactionId = UUID.randomUUID().toString();
        eventBus.publish(new CreditAccount(transactionId, account.id, BigDecimal.TEN, account.id));

        eventTestSubscriber
                .awaitCount(1)
                .assertNoErrors()
                .assertValueCount(1)
                .assertValue(new AccountCreditedSuccessfully(account.id, transactionId, BigDecimal.TEN, account.id));
    }

    @Test
    void should_push_debited_successfully_event_on_transfer() {
        var eventTestSubscriber = eventBus.observeEvents(Event.class).test();

        var accountA = accountsRepository.save(Account.create("email"));
        var accountB = accountsRepository.save(Account.create("email"));
        var creditTransactionId = UUID.randomUUID().toString();
        var debitTransactionId = UUID.randomUUID().toString();
        eventBus.publish(new CreditAccount(creditTransactionId, accountA.id, BigDecimal.TEN, accountA.id));
        eventBus.publish(new DebitAccount(debitTransactionId, accountA.id, BigDecimal.ONE, accountB.id));

        eventTestSubscriber
                .awaitCount(2)
                .assertNoErrors()
                .assertValueCount(2)
                .assertValues(
                        new AccountCreditedSuccessfully(accountA.id, creditTransactionId, BigDecimal.TEN, accountA.id),
                        new AccountDebitedSuccessfully(accountA.id, debitTransactionId, BigDecimal.ONE, accountB.id)
                );
    }

    @Test
    void should_push_debited_successfully_event_on_withdraw() {
        var eventTestSubscriber = eventBus.observeEvents(Event.class).test();

        var accountA = accountsRepository.save(Account.create("email"));
        var creditTransactionId = UUID.randomUUID().toString();
        var debitTransactionId = UUID.randomUUID().toString();
        eventBus.publish(new CreditAccount(creditTransactionId, accountA.id, BigDecimal.TEN, accountA.id));
        eventBus.publish(new DebitAccount(debitTransactionId, accountA.id, BigDecimal.TEN, accountA.id));

        eventTestSubscriber
                .awaitCount(2)
                .assertNoErrors()
                .assertValueCount(2)
                .assertValues(
                        new AccountCreditedSuccessfully(accountA.id, creditTransactionId, BigDecimal.TEN, accountA.id),
                        new AccountDebitedSuccessfully(accountA.id, debitTransactionId, BigDecimal.TEN, accountA.id)
                );
    }

    @Test
    void should_push_account_does_not_exists_event_on_credit() {
        var eventTestSubscriber = eventBus.observeEvents(Event.class).test();

        var notExistingAccountId = UUID.randomUUID().toString();
        var transactionId = UUID.randomUUID().toString();
        eventBus.publish(new CreditAccount(transactionId, notExistingAccountId, BigDecimal.TEN, notExistingAccountId));

        eventTestSubscriber
                .awaitCount(1)
                .assertNoErrors()
                .assertValueCount(1)
                .assertValue(new AccountDoesNotExists(new CreditAccount(transactionId, notExistingAccountId, BigDecimal.TEN, notExistingAccountId)));
    }

    @Test
    void should_push_account_does_not_exists_event_on_debit() {
        var eventTestSubscriber = eventBus.observeEvents(Event.class).test();

        var accountA = accountsRepository.save(Account.create("email"));
        var notExistingAccountId = UUID.randomUUID().toString();
        var debitTransactionId = UUID.randomUUID().toString();
        eventBus.publish(new DebitAccount(debitTransactionId, notExistingAccountId, BigDecimal.TEN, accountA.id));

        eventTestSubscriber
                .awaitCount(1)
                .assertNoErrors()
                .assertValueCount(1)
                .assertValue(new AccountDoesNotExists(new DebitAccount(debitTransactionId, notExistingAccountId, BigDecimal.TEN, accountA.id)));
    }

    @Test
    void should_push_transaction_failed_event_on_credit_when_amount_is_invalid() {
        var eventTestSubscriber = eventBus.observeEvents(Event.class).test();

        var accountA = accountsRepository.save(Account.create("email"));
        var creditTransactionId = UUID.randomUUID().toString();
        eventBus.publish(new CreditAccount(creditTransactionId, accountA.id, new BigDecimal(-1), accountA.id));

        eventTestSubscriber
                .awaitCount(1)
                .assertNoErrors()
                .assertValueCount(1)
                .assertValue(new TransactionFailed(creditTransactionId, accountA.id, "invalid_amount"));
    }

    @Test
    void should_push_transaction_failed_event_on_debit_when_amount_is_invalid() {
        var eventTestSubscriber = eventBus.observeEvents(Event.class).test();

        var accountA = accountsRepository.save(Account.create("email"));
        var debitTransactionId = UUID.randomUUID().toString();
        eventBus.publish(new DebitAccount(debitTransactionId, accountA.id, new BigDecimal(-1), accountA.id));

        eventTestSubscriber
                .awaitCount(1)
                .assertNoErrors()
                .assertValueCount(1)
                .assertValue(new TransactionFailed(debitTransactionId, accountA.id, "invalid_amount"));
    }
}