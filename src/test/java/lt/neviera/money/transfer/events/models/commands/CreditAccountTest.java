package lt.neviera.money.transfer.events.models.commands;

import org.junit.jupiter.api.*;

import java.math.BigDecimal;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
class CreditAccountTest {

    @Test
    void should_recognize_as_top_up_when_issuer_is_the_same() {
        var accountId = UUID.randomUUID().toString();
        var creditAccount = new CreditAccount(UUID.randomUUID().toString(), accountId, BigDecimal.ONE, accountId);
        assertTrue(creditAccount.isTopUp());
    }

    @Test
    void should_recognize_as_transfer_when_issuer_is_different() {
        var creditAccount = new CreditAccount(UUID.randomUUID().toString(), UUID.randomUUID().toString(), BigDecimal.ONE, UUID.randomUUID().toString());
        assertFalse(creditAccount.isTopUp());
    }

    @Test
    void should_reverse_issuer_with_main_account_on_refund() {
        var transactionId = UUID.randomUUID().toString();
        var accountId = UUID.randomUUID().toString();
        var issuerAccountId = UUID.randomUUID().toString();
        var creditAccount = new CreditAccount(transactionId, accountId, BigDecimal.ONE, issuerAccountId);
        var refundTransaction = creditAccount.refundTransaction();
        assertEquals(transactionId + "_REFUND", refundTransaction.id);
        assertEquals(BigDecimal.ONE, refundTransaction.amount);
        assertEquals(issuerAccountId, refundTransaction.accountId);
        assertEquals(accountId, refundTransaction.issuerAccountId);
    }
}