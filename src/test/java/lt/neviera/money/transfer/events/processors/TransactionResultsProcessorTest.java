package lt.neviera.money.transfer.events.processors;

import io.reactivex.disposables.Disposable;
import lt.neviera.money.transfer.events.EventBus;
import lt.neviera.money.transfer.events.models.Command;
import lt.neviera.money.transfer.events.models.commands.CreditAccount;
import lt.neviera.money.transfer.events.models.events.*;
import org.junit.jupiter.api.*;

import java.math.BigDecimal;
import java.util.UUID;

@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
class TransactionResultsProcessorTest {

    private EventBus eventBus;
    private Disposable disposable;

    @BeforeEach
    void setUp() {
        eventBus = new EventBus();
        var transactionsProcessor = new TransactionResultsProcessor();
        disposable = transactionsProcessor.subscribe(eventBus);
    }

    @AfterEach
    void tearDown() {
        disposable.dispose();
        eventBus.shutdown();
    }

    @Test
    void should_emit_credit_command_on_successful_transfer() {
        var transactionResultTestSubscriber = eventBus.observeCommands(Command.class).test();

        var transactionId = UUID.randomUUID().toString();
        var senderAccountId = UUID.randomUUID().toString();
        var receiverAccountId = UUID.randomUUID().toString();

        eventBus.publish(new AccountDebitedSuccessfully(senderAccountId, transactionId, BigDecimal.TEN, receiverAccountId));

        transactionResultTestSubscriber
                .awaitCount(1)
                .assertNoErrors()
                .assertValueCount(1)
                .assertValue(new CreditAccount(transactionId, receiverAccountId, BigDecimal.TEN, senderAccountId));
    }

    @Test
    void should_not_credit_command_on_successful_withdraw() {
        var transactionResultTestSubscriber = eventBus.observeCommands(Command.class).test();

        var transactionId = UUID.randomUUID().toString();
        var accountId = UUID.randomUUID().toString();

        eventBus.publish(new AccountDebitedSuccessfully(accountId, transactionId, BigDecimal.TEN));

        transactionResultTestSubscriber
                .assertNoErrors()
                .assertNoValues();
    }

    @Test
    void should_emit_refund_credit_command_when_account_is_missing_during_transfer() {
        var transactionResultTestSubscriber = eventBus.observeCommands(Command.class).test();

        var transactionId = UUID.randomUUID().toString();
        var senderAccountId = UUID.randomUUID().toString();
        var receiverAccountId = UUID.randomUUID().toString();

        eventBus.publish(new AccountDoesNotExists(new CreditAccount(transactionId, senderAccountId, BigDecimal.TEN, receiverAccountId)));

        transactionResultTestSubscriber
                .awaitCount(1)
                .assertNoErrors()
                .assertValueCount(1)
                .assertValue(new CreditAccount(transactionId + "_REFUND", receiverAccountId, BigDecimal.TEN, senderAccountId));
    }
}