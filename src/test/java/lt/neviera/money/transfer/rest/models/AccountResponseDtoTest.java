package lt.neviera.money.transfer.rest.models;

import lt.neviera.money.transfer.domain.models.Account;
import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
class AccountResponseDtoTest {

    @Test
    void should_map_to_account_dto_without_transactions() {
        var account = Account.create("email");
        var accountResponseDto = AccountResponseDto.from(account);
        assertEquals(account.id, accountResponseDto.id);
        assertEquals(account.email, accountResponseDto.email);
        assertEquals(account.balance, accountResponseDto.balance);
        assertEquals(0, account.allTransactions().size());
    }
}