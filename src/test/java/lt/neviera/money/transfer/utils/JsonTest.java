package lt.neviera.money.transfer.utils;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
class JsonTest {

    private static final String TEST_STRING = "{\"id\":1,\"name\":\"value\"}";

    @Test
    void should_parse_object_to_json_string() {
        assertEquals(TEST_STRING, Json.toJsonString(new TestDto(1, "value")));
    }

    @Test
    void should_deserialize_object_from_string() {
        assertEquals(new TestDto(1, "value"), Json.deserialize(TEST_STRING, TestDto.class));
    }

    private static final class TestDto {
        public final int id;
        public final String name;

        public TestDto(int id, String name) {
            this.id = id;
            this.name = name;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            var testDto = (TestDto) o;
            if (id != testDto.id) return false;
            return name.equals(testDto.name);
        }

        @Override
        public int hashCode() {
            int result = id;
            result = 31 * result + name.hashCode();
            return result;
        }
    }
}