package lt.neviera.money.transfer;

import io.reactivex.disposables.Disposable;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpServer;
import lt.neviera.money.transfer.domain.models.transaction.FailedTransaction;
import lt.neviera.money.transfer.events.EventBus;
import lt.neviera.money.transfer.events.processors.*;
import lt.neviera.money.transfer.repositories.*;
import lt.neviera.money.transfer.rest.models.*;
import lt.neviera.money.transfer.utils.Json;
import org.junit.jupiter.api.*;

import java.math.BigDecimal;
import java.net.URI;
import java.net.http.*;
import java.util.*;

import static java.net.http.HttpRequest.BodyPublishers.ofString;
import static lt.neviera.money.transfer.utils.Json.toJsonString;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.instanceOf;
import static org.junit.jupiter.api.Assertions.*;

@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class WorkflowIntegrationTest {

    private static final HttpClient HTTP_CLIENT = HttpClient.newBuilder().build();

    private static final int TEST_APP_PORT = 7777;
    private static final URI API_BASE_PATH = URI.create("http://localhost:" + TEST_APP_PORT);

    private EventBus eventBus;
    private AccountsRepository accountsRepository;
    private HttpServer httpServer;
    private List<Disposable> disposables = new ArrayList<>();

    @BeforeEach
    void setUp() {
        eventBus = new EventBus();
        accountsRepository = new InMemoryAccountsRepository();
        disposables.addAll(List.of(
                new TransactionsProcessor(accountsRepository).subscribe(eventBus),
                new TransactionResultsProcessor().subscribe(eventBus)
        ));
        httpServer = Application.initHttpServer(Vertx.vertx(), eventBus, accountsRepository, TEST_APP_PORT);
    }

    @AfterEach
    void tearDown() {
        eventBus.shutdown();
        disposables.forEach(Disposable::dispose);
        httpServer.close();
    }

    @Test
    void should_open_new_account() throws Exception {
        var openAccountRequest = buildOpenAccountRequest("first@email.com");
        var response = HTTP_CLIENT.send(openAccountRequest, HttpResponse.BodyHandlers.ofString());
        var createdAccount = Json.deserialize(response.body(), AccountResponseDto.class);

        assertEquals("first@email.com", createdAccount.email);
        assertEquals(1, accountsRepository.findAll().size());

        accountsRepository.find(createdAccount.id).ifPresentOrElse(a -> {
            assertEquals(createdAccount.id, a.id);
            assertEquals(createdAccount.email, a.email);
            assertEquals(BigDecimal.ZERO, a.balance);
            assertEquals(0, a.allTransactions().size());
        }, () -> fail("can't find specified account"));
    }

    @Test
    void should_return_all_created_accounts() throws Exception {
        var openFirstAccountRequest = buildOpenAccountRequest("first@email.com");
        HTTP_CLIENT.send(openFirstAccountRequest, HttpResponse.BodyHandlers.ofString());

        var openSecondAccountRequest = buildOpenAccountRequest("second@email.com");
        HTTP_CLIENT.send(openSecondAccountRequest, HttpResponse.BodyHandlers.ofString());

        var getAccountsRequest = HttpRequest.newBuilder(API_BASE_PATH.resolve("/account"))
                .GET()
                .setHeader("Content-Type", "application/json")
                .build();

        var response = HTTP_CLIENT.send(getAccountsRequest, HttpResponse.BodyHandlers.ofString());
        var accounts = Json.deserialize(response.body(), AccountResponseDto[].class);

        assertEquals(2, accounts.length);
    }

    @Test
    void should_return_created_account() throws Exception {
        var openAccountRequest = buildOpenAccountRequest("first@email.com");
        var response = HTTP_CLIENT.send(openAccountRequest, HttpResponse.BodyHandlers.ofString());
        var createdAccount = Json.deserialize(response.body(), AccountResponseDto.class);

        var getAccountRequest = HttpRequest.newBuilder(API_BASE_PATH.resolve("/account/" + createdAccount.id))
                .GET()
                .setHeader("Content-Type", "application/json")
                .build();

        var accountResponse = HTTP_CLIENT.send(getAccountRequest, HttpResponse.BodyHandlers.ofString());
        var returnedAccount = Json.deserialize(accountResponse.body(), AccountResponseDto.class);

        assertEquals(createdAccount.id, returnedAccount.id);
        assertEquals(createdAccount.email, returnedAccount.email);
        assertEquals(createdAccount.balance, returnedAccount.balance);
    }

    @Test
    void should_close_existing_account() throws Exception {
        var openRequest = buildOpenAccountRequest("first@email.com");

        var response = HTTP_CLIENT.send(openRequest, HttpResponse.BodyHandlers.ofString());
        var createdAccount = Json.deserialize(response.body(), AccountResponseDto.class);

        assertEquals(1, accountsRepository.findAll().size());

        var closeAccountRequest = HttpRequest.newBuilder(API_BASE_PATH.resolve("/account/" + createdAccount.id))
                .DELETE()
                .setHeader("Content-Type", "application/json")
                .build();

        var closeResponse = HTTP_CLIENT.send(closeAccountRequest, HttpResponse.BodyHandlers.ofString());

        assertEquals(204, closeResponse.statusCode());
        assertEquals(0, accountsRepository.findAll().size());
    }

    @Test
    void should_top_up_created_account() throws Exception {
        var openAccountRequest = buildOpenAccountRequest("first@email.com");

        var response = HTTP_CLIENT.send(openAccountRequest, HttpResponse.BodyHandlers.ofString());
        var createdAccount = Json.deserialize(response.body(), AccountResponseDto.class);

        accountsRepository.find(createdAccount.id).ifPresentOrElse(a -> {
            assertEquals(createdAccount.id, a.id);
            assertEquals(createdAccount.email, a.email);
            assertEquals(BigDecimal.ZERO, a.balance);
            assertEquals(0, a.allTransactions().size());
        }, () -> fail("can't find specified account"));

        var topUpRequest = HttpRequest.newBuilder(API_BASE_PATH.resolve("/account/" + createdAccount.id + "/topup"))
                .POST(ofString(toJsonString(new TopUpRequestDto(BigDecimal.TEN))))
                .setHeader("Content-Type", "application/json")
                .build();

        var topUpResponse = HTTP_CLIENT.send(topUpRequest, HttpResponse.BodyHandlers.ofString());

        assertEquals(202, topUpResponse.statusCode());

        waitForTransactionCount(createdAccount.id, 1);

        accountsRepository.find(createdAccount.id).ifPresentOrElse(a -> {
                assertEquals(createdAccount.id, a.id);
                assertEquals(createdAccount.email, a.email);
                assertEquals(BigDecimal.TEN, a.balance);
                assertEquals(1, a.allTransactions().size());
        }, () -> fail("can't find specified account"));
    }

    @Test
    void should_return_bad_request_if_top_up_with_negative_amount() throws Exception {
        var openAccountRequest = buildOpenAccountRequest("first@email.com");

        var response = HTTP_CLIENT.send(openAccountRequest, HttpResponse.BodyHandlers.ofString());
        var createdAccount = Json.deserialize(response.body(), AccountResponseDto.class);

        var topUpRequest = HttpRequest.newBuilder(API_BASE_PATH.resolve("/account/" + createdAccount.id + "/topup"))
                .POST(ofString(toJsonString(new TopUpRequestDto(new BigDecimal(-1)))))
                .setHeader("Content-Type", "application/json")
                .build();

        var topUpResponse = HTTP_CLIENT.send(topUpRequest, HttpResponse.BodyHandlers.ofString());

        assertEquals(400, topUpResponse.statusCode());
    }

    @Test
    void should_return_bad_request_if_top_up_from_not_existing_account() throws Exception {
        var topUpRequest = HttpRequest.newBuilder(API_BASE_PATH.resolve("/account/notExistingId/topup"))
                .POST(ofString(toJsonString(new TopUpRequestDto(BigDecimal.ONE))))
                .setHeader("Content-Type", "application/json")
                .build();

        var topUpResponse = HTTP_CLIENT.send(topUpRequest, HttpResponse.BodyHandlers.ofString());

        assertEquals(400, topUpResponse.statusCode());
    }

    @Test
    void should_withdraw_money_from_account() throws Exception {
        var openAccountRequest = buildOpenAccountRequest("first@email.com");

        var response = HTTP_CLIENT.send(openAccountRequest, HttpResponse.BodyHandlers.ofString());
        var createdAccount = Json.deserialize(response.body(), AccountResponseDto.class);

        var topUpRequest = HttpRequest.newBuilder(API_BASE_PATH.resolve("/account/" + createdAccount.id + "/topup"))
                .POST(ofString(toJsonString(new TopUpRequestDto(BigDecimal.TEN))))
                .setHeader("Content-Type", "application/json")
                .build();

        HTTP_CLIENT.send(topUpRequest, HttpResponse.BodyHandlers.ofString());

        var withdrawRequest = HttpRequest.newBuilder(API_BASE_PATH.resolve("/account/" + createdAccount.id + "/withdraw"))
                .POST(ofString(toJsonString(new WithdrawRequestDto(BigDecimal.ONE))))
                .setHeader("Content-Type", "application/json")
                .build();

        var withdrawResponse = HTTP_CLIENT.send(withdrawRequest, HttpResponse.BodyHandlers.ofString());

        assertEquals(202, withdrawResponse.statusCode());

        waitForTransactionCount(createdAccount.id, 2);

        accountsRepository.find(createdAccount.id).ifPresentOrElse(a -> {
            assertEquals(createdAccount.id, a.id);
            assertEquals(createdAccount.email, a.email);
            assertEquals(new BigDecimal(9), a.balance);
            assertEquals(2, a.allTransactions().size());
        }, () -> fail("can't find specified account"));
    }

    @Test
    void should_return_bad_request_if_withdraw_with_negative_amount() throws Exception {
        var openAccountRequest = buildOpenAccountRequest("first@email.com");

        var response = HTTP_CLIENT.send(openAccountRequest, HttpResponse.BodyHandlers.ofString());
        var createdAccount = Json.deserialize(response.body(), AccountResponseDto.class);

        var withdrawRequest = HttpRequest.newBuilder(API_BASE_PATH.resolve("/account/" + createdAccount.id + "/withdraw"))
                .POST(ofString(toJsonString(new WithdrawRequestDto(new BigDecimal(-1)))))
                .setHeader("Content-Type", "application/json")
                .build();

        var withdrawResponse = HTTP_CLIENT.send(withdrawRequest, HttpResponse.BodyHandlers.ofString());

        assertEquals(400, withdrawResponse.statusCode());
    }

    @Test
    void should_return_bad_request_if_withdraw_from_not_existing_account() throws Exception {
        var withdrawRequest = HttpRequest.newBuilder(API_BASE_PATH.resolve("/account/notExistingId/withdraw"))
                .POST(ofString(toJsonString(new WithdrawRequestDto(BigDecimal.ONE))))
                .setHeader("Content-Type", "application/json")
                .build();

        var withdrawResponse = HTTP_CLIENT.send(withdrawRequest, HttpResponse.BodyHandlers.ofString());

        assertEquals(400, withdrawResponse.statusCode());
    }

    @Test
    void should_not_withdraw_money_from_empty_account() throws Exception {
        var openAccountRequest = buildOpenAccountRequest("first@email.com");

        var response = HTTP_CLIENT.send(openAccountRequest, HttpResponse.BodyHandlers.ofString());
        var createdAccount = Json.deserialize(response.body(), AccountResponseDto.class);

        var withdrawRequest = HttpRequest.newBuilder(API_BASE_PATH.resolve("/account/" + createdAccount.id + "/withdraw"))
                .POST(ofString(toJsonString(new WithdrawRequestDto(BigDecimal.ONE))))
                .setHeader("Content-Type", "application/json")
                .build();

        var withdrawResponse = HTTP_CLIENT.send(withdrawRequest, HttpResponse.BodyHandlers.ofString());

        assertEquals(202, withdrawResponse.statusCode());

        waitForTransactionCount(createdAccount.id, 1);

        accountsRepository.find(createdAccount.id).ifPresentOrElse(a -> {
            assertEquals(createdAccount.id, a.id);
            assertEquals(createdAccount.email, a.email);
            assertEquals(BigDecimal.ZERO, a.balance);
            assertEquals(1, a.allTransactions().size());
            a.allTransactions().stream().findFirst()
                    .ifPresent(t -> assertThat(t.status, instanceOf(FailedTransaction.class)));
        }, () -> fail("can't find specified account"));
    }

    @Test
    void should_transfer_money_to_another_account() throws Exception {
        var openFirstAccountRequest = buildOpenAccountRequest("first@email.com");

        var response = HTTP_CLIENT.send(openFirstAccountRequest, HttpResponse.BodyHandlers.ofString());
        var firstAccount = Json.deserialize(response.body(), AccountResponseDto.class);

        var topUpRequest = HttpRequest.newBuilder(API_BASE_PATH.resolve("/account/" + firstAccount.id + "/topup"))
                .POST(ofString(toJsonString(new TopUpRequestDto(BigDecimal.TEN))))
                .setHeader("Content-Type", "application/json")
                .build();

        HTTP_CLIENT.send(topUpRequest, HttpResponse.BodyHandlers.ofString());

        var openSecondAccountRequest = buildOpenAccountRequest("second@email.com");

        var secondAccountResponse = HTTP_CLIENT.send(openSecondAccountRequest, HttpResponse.BodyHandlers.ofString());
        var secondAccount = Json.deserialize(secondAccountResponse.body(), AccountResponseDto.class);

        assertEquals(2, accountsRepository.findAll().size());

        var transferRequest = HttpRequest.newBuilder(API_BASE_PATH.resolve("/account/" + firstAccount.id + "/transfer"))
                .POST(ofString(toJsonString(new TransferRequestDto(BigDecimal.ONE, secondAccount.id))))
                .setHeader("Content-Type", "application/json")
                .build();

        HTTP_CLIENT.send(transferRequest, HttpResponse.BodyHandlers.ofString());

        waitForTransactionCount(firstAccount.id, 2);

        accountsRepository.find(firstAccount.id).ifPresentOrElse(a -> {
            assertEquals(firstAccount.id, a.id);
            assertEquals(firstAccount.email, a.email);
            assertEquals(new BigDecimal(9), a.balance);
            assertEquals(2, a.allTransactions().size());
        }, () -> fail("can't find specified account"));

        waitForTransactionCount(secondAccount.id, 1);

        accountsRepository.find(secondAccount.id).ifPresentOrElse(a -> {
            assertEquals(secondAccount.id, a.id);
            assertEquals(secondAccount.email, a.email);
            assertEquals(new BigDecimal(1), a.balance);
            assertEquals(1, a.allTransactions().size());
        }, () -> fail("can't find specified account"));
    }

    @Test
    void should_return_bad_request_if_transferring_to_not_existing_account() throws Exception {
        var openFirstAccountRequest = buildOpenAccountRequest("first@email.com");

        var response = HTTP_CLIENT.send(openFirstAccountRequest, HttpResponse.BodyHandlers.ofString());
        var firstAccount = Json.deserialize(response.body(), AccountResponseDto.class);

        var topUpRequest = HttpRequest.newBuilder(API_BASE_PATH.resolve("/account/" + firstAccount.id + "/topup"))
                .POST(ofString(toJsonString(new TopUpRequestDto(BigDecimal.TEN))))
                .setHeader("Content-Type", "application/json")
                .build();

        HTTP_CLIENT.send(topUpRequest, HttpResponse.BodyHandlers.ofString());

        var transferRequest = HttpRequest.newBuilder(API_BASE_PATH.resolve("/account/" + firstAccount.id + "/transfer"))
                .POST(ofString(toJsonString(new TransferRequestDto(BigDecimal.ONE, "notExistingId"))))
                .setHeader("Content-Type", "application/json")
                .build();

        var transferResponse = HTTP_CLIENT.send(transferRequest, HttpResponse.BodyHandlers.ofString());

        assertEquals(400, transferResponse.statusCode());
    }

    @Test
    void should_not_transfer_more_money_than_exists_in_account() throws Exception {
        var openFirstAccountRequest = buildOpenAccountRequest("first@email.com");

        var response = HTTP_CLIENT.send(openFirstAccountRequest, HttpResponse.BodyHandlers.ofString());
        var firstAccount = Json.deserialize(response.body(), AccountResponseDto.class);

        var topUpRequest = HttpRequest.newBuilder(API_BASE_PATH.resolve("/account/" + firstAccount.id + "/topup"))
                .POST(ofString(toJsonString(new TopUpRequestDto(BigDecimal.ONE))))
                .setHeader("Content-Type", "application/json")
                .build();

        HTTP_CLIENT.send(topUpRequest, HttpResponse.BodyHandlers.ofString());

        var openSecondAccountRequest = buildOpenAccountRequest("second@email.com");

        var secondAccountResponse = HTTP_CLIENT.send(openSecondAccountRequest, HttpResponse.BodyHandlers.ofString());
        var secondAccount = Json.deserialize(secondAccountResponse.body(), AccountResponseDto.class);

        assertEquals(2, accountsRepository.findAll().size());

        var transferRequest = HttpRequest.newBuilder(API_BASE_PATH.resolve("/account/" + firstAccount.id + "/transfer"))
                .POST(ofString(toJsonString(new TransferRequestDto(BigDecimal.TEN, secondAccount.id))))
                .setHeader("Content-Type", "application/json")
                .build();

        var transferResponse = HTTP_CLIENT.send(transferRequest, HttpResponse.BodyHandlers.ofString());
        var transferTransaction = Json.deserialize(transferResponse.body(), CreatedTransactionResponseDto.class);

        waitForTransactionCount(firstAccount.id, 2);

        accountsRepository.find(firstAccount.id).ifPresentOrElse(a -> {
            assertEquals(firstAccount.id, a.id);
            assertEquals(firstAccount.email, a.email);
            assertEquals(BigDecimal.ONE, a.balance);
            assertEquals(2, a.allTransactions().size());
            a.allTransactions().stream()
                    .filter(t -> t.id.equals(transferTransaction.transactionId))
                    .findFirst()
                    .ifPresentOrElse(tr -> assertThat(tr.status, instanceOf(FailedTransaction.class)),
                            () -> fail("can't find transfer transaction"));
        }, () -> fail("can't find specified account"));

        accountsRepository.find(secondAccount.id).ifPresentOrElse(a -> {
            assertEquals(secondAccount.id, a.id);
            assertEquals(secondAccount.email, a.email);
            assertEquals(BigDecimal.ZERO, a.balance);
            assertEquals(0, a.allTransactions().size());
        }, () -> fail("can't find specified account"));
    }

    @Test
    void should_return_bad_request_if_transferring_to_the_same_account() throws Exception {
        var openFirstAccountRequest = buildOpenAccountRequest("first@email.com");

        var response = HTTP_CLIENT.send(openFirstAccountRequest, HttpResponse.BodyHandlers.ofString());
        var firstAccount = Json.deserialize(response.body(), AccountResponseDto.class);

        var topUpRequest = HttpRequest.newBuilder(API_BASE_PATH.resolve("/account/" + firstAccount.id + "/topup"))
                .POST(ofString(toJsonString(new TopUpRequestDto(BigDecimal.TEN))))
                .setHeader("Content-Type", "application/json")
                .build();

        HTTP_CLIENT.send(topUpRequest, HttpResponse.BodyHandlers.ofString());

        var transferRequest = HttpRequest.newBuilder(API_BASE_PATH.resolve("/account/" + firstAccount.id + "/transfer"))
                .POST(ofString(toJsonString(new TransferRequestDto(BigDecimal.ONE, firstAccount.id))))
                .setHeader("Content-Type", "application/json")
                .build();

        var transferResponse = HTTP_CLIENT.send(transferRequest, HttpResponse.BodyHandlers.ofString());

        assertEquals(400, transferResponse.statusCode());
    }

    private HttpRequest buildOpenAccountRequest(String email) {
        return HttpRequest.newBuilder(API_BASE_PATH.resolve("/account"))
                .POST(ofString(toJsonString(new OpenAccountRequestDto(email))))
                .setHeader("Content-Type", "application/json")
                .build();
    }

    private void waitForTransactionCount(String accountId, int transactionCount) {
        while (accountsRepository.find(accountId)
                .orElseThrow(() -> new RuntimeException("account does not exists"))
                .allTransactions()
                .size() != transactionCount) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
