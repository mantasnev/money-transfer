package lt.neviera.money.transfer.domain.models;

import lt.neviera.money.transfer.domain.models.transaction.*;
import org.junit.jupiter.api.*;

import java.math.BigDecimal;
import java.util.*;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.instanceOf;
import static org.junit.jupiter.api.Assertions.*;

@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
class AccountTest {

    @Test
    void should_create_empty_account() {
        var account = Account.create("email");
        assertEquals(0, account.allTransactions().size());
        assertEquals(BigDecimal.ZERO, account.balance);
    }

    @Test
    void should_credit_empty_account() {
        var transactionId = UUID.randomUUID().toString();
        var account = Account.create("email").credit(transactionId, new BigDecimal(7.17));
        assertEquals(new BigDecimal(7.17), account.balance);
        assertEquals(1, account.allTransactions().size());
        account.findTransactionBy(transactionId).ifPresentOrElse(t -> {
            assertEquals(transactionId, t.id);
            assertEquals(new BigDecimal(7.17), t.amount);
            assertThat(t.status, instanceOf(SuccessfulTransaction.class));
            assertEquals(TransactionType.CREDIT, t.type);
            assertEquals(account.id, t.issuerAccountId);
        }, () -> fail("can't find executed credit transaction"));
    }

    @Test
    void should_credit_with_specific_issuer_account_id() {
        var issuerAccountId = UUID.randomUUID().toString();
        var transactionId = UUID.randomUUID().toString();
        var account = Account.create("email").credit(transactionId, BigDecimal.ONE, issuerAccountId);
        account.findTransactionBy(transactionId).ifPresentOrElse(t -> {
            assertEquals(transactionId, t.id);
            assertEquals(issuerAccountId, t.issuerAccountId);
        }, () -> fail("can't find executed credit transaction"));
    }

    @Test
    void should_debit_credited_account() {
        var creditTransactionId = UUID.randomUUID().toString();
        var debitTransactionId = UUID.randomUUID().toString();
        var account = Account.create("email")
                .credit(creditTransactionId, BigDecimal.TEN)
                .debit(debitTransactionId, BigDecimal.ONE);
        assertEquals(new BigDecimal(9), account.balance);
        assertEquals(2, account.allTransactions().size());

        account.findTransactionBy(debitTransactionId).ifPresentOrElse(t -> {
            assertEquals(debitTransactionId, t.id);
            assertEquals(BigDecimal.ONE, t.amount);
            assertThat(t.status, instanceOf(SuccessfulTransaction.class));
            assertEquals(TransactionType.DEBIT, t.type);
            assertEquals(account.id, t.issuerAccountId);
        }, () -> fail("can't find executed debit transaction"));
    }

    @Test
    void credit_should_be_idempotent() {
        var transactionId = UUID.randomUUID().toString();
        var account = Account.create("email")
                .credit(UUID.randomUUID().toString(), BigDecimal.TEN)
                .credit(transactionId, BigDecimal.ONE)
                .credit(transactionId, BigDecimal.ONE)
                .credit(transactionId, BigDecimal.ONE)
                .credit(transactionId, BigDecimal.ONE)
                .credit(transactionId, BigDecimal.ONE);
        assertEquals(new BigDecimal(11), account.balance);
        assertEquals(2, account.allTransactions().size());
    }

    @Test
    void debit_should_be_idempotent() {
        var transactionId = UUID.randomUUID().toString();
        var account = Account.create("email")
                .credit(UUID.randomUUID().toString(), BigDecimal.TEN)
                .debit(transactionId, BigDecimal.ONE)
                .debit(transactionId, BigDecimal.ONE)
                .debit(transactionId, BigDecimal.ONE)
                .debit(transactionId, BigDecimal.ONE)
                .debit(transactionId, BigDecimal.ONE);
        assertEquals(new BigDecimal(9), account.balance);
        assertEquals(2, account.allTransactions().size());
    }

    @Test
    void should_not_credit_zero() {
        var transactionId = UUID.randomUUID().toString();
        var account = Account.create("email").credit(transactionId, BigDecimal.ZERO);
        assertEquals(BigDecimal.ZERO, account.balance);
        assertEquals(1, account.allTransactions().size());
        account.findTransactionBy(transactionId).ifPresentOrElse(t -> {
            assertEquals(transactionId, t.id);
            assertEquals(new BigDecimal(0), t.amount);
            assertThat(t.status, instanceOf(FailedTransaction.class));
            assertEquals(FailedTransaction.INVALID_AMOUNT, t.status);
            assertEquals(TransactionType.CREDIT, t.type);
            assertEquals(account.id, t.issuerAccountId);
        }, () -> fail("can't find executed credit transaction"));
    }

    @Test
    void should_not_credit_negative_amount() {
        var transactionId = UUID.randomUUID().toString();
        var account = Account.create("email").credit(transactionId, new BigDecimal(-1));
        assertEquals(BigDecimal.ZERO, account.balance);
        assertEquals(1, account.allTransactions().size());
        account.findTransactionBy(transactionId).ifPresentOrElse(t -> {
            assertEquals(transactionId, t.id);
            assertEquals(new BigDecimal(-1), t.amount);
            assertThat(t.status, instanceOf(FailedTransaction.class));
            assertEquals(FailedTransaction.INVALID_AMOUNT, t.status);
            assertEquals(TransactionType.CREDIT, t.type);
            assertEquals(account.id, t.issuerAccountId);
        }, () -> fail("can't find executed credit transaction"));
    }

    @Test
    void should_not_debit_zero() {
        var transactionId = UUID.randomUUID().toString();
        var account = Account.create("email").debit(transactionId, BigDecimal.ZERO);
        assertEquals(BigDecimal.ZERO, account.balance);
        assertEquals(1, account.allTransactions().size());
        account.findTransactionBy(transactionId).ifPresentOrElse(t -> {
            assertEquals(transactionId, t.id);
            assertEquals(new BigDecimal(0), t.amount);
            assertThat(t.status, instanceOf(FailedTransaction.class));
            assertEquals(FailedTransaction.INVALID_AMOUNT, t.status);
            assertEquals(TransactionType.DEBIT, t.type);
            assertEquals(account.id, t.issuerAccountId);
        }, () -> fail("can't find executed debit transaction"));
    }

    @Test
    void should_not_debit_negative_amount() {
        var transactionId = UUID.randomUUID().toString();
        var account = Account.create("email").debit(transactionId, new BigDecimal(-1));
        assertEquals(BigDecimal.ZERO, account.balance);
        assertEquals(1, account.allTransactions().size());
        account.findTransactionBy(transactionId).ifPresentOrElse(t -> {
            assertEquals(transactionId, t.id);
            assertEquals(new BigDecimal(-1), t.amount);
            assertThat(t.status, instanceOf(FailedTransaction.class));
            assertEquals(FailedTransaction.INVALID_AMOUNT, t.status);
            assertEquals(TransactionType.DEBIT, t.type);
            assertEquals(account.id, t.issuerAccountId);
        }, () -> fail("can't find executed debit transaction"));
    }

    @Test
    void should_not_debit_when_balance_is_insufficient() {
        var transactionId = UUID.randomUUID().toString();
        var account = Account.create("email")
                .credit(UUID.randomUUID().toString(), BigDecimal.ONE)
                .debit(transactionId, BigDecimal.TEN);
        assertEquals(BigDecimal.ONE, account.balance);
        assertEquals(2, account.allTransactions().size());
        account.findTransactionBy(transactionId).ifPresentOrElse(t -> {
            assertEquals(transactionId, t.id);
            assertEquals(BigDecimal.TEN, t.amount);
            assertThat(t.status, instanceOf(FailedTransaction.class));
            assertEquals(FailedTransaction.INSUFFICIENT_FUNDS, t.status);
            assertEquals(TransactionType.DEBIT, t.type);
            assertEquals(account.id, t.issuerAccountId);
        }, () -> fail("can't find executed debit transaction"));
    }
}