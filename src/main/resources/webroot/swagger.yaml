swagger: '2.0'
info:
  title: Money Tranfer API
  version: "1.0"
  contact:
    name: Mantas Neviera
    url: http://www.neviera.lt
    email: mantasneviera@gmail.com
host: localhost:7777
basePath: /
tags:
  - name: accounts-controller
paths:
  /account:
    get:
      tags:
        - accounts-controller
      summary: Get all accounts
      produces:
        - application/json
      responses:
        '200':
          description: Accounts successfully returned
          schema:
            type: array
            items:
              $ref: '#/definitions/AccountResponseDto'
        '500':
          description: Internal server error
          schema:
            $ref: '#/definitions/FailedResponseDto'
      deprecated: false
    post:
      tags:
        - accounts-controller
      summary: Open new account
      consumes:
        - application/json
      produces:
        - application/json
      parameters:
        - in: body
          name: accountRequestDto
          description: accountRequestDto
          required: true
          schema:
            $ref: '#/definitions/OpenAccountRequestDto'
      responses:
        '201':
          description: Account successfully opened
          schema:
            $ref: '#/definitions/AccountResponseDto'
        '400':
          description: Invalid request
          schema:
            $ref: '#/definitions/FailedResponseDto'
        '500':
          description: Internal server error
          schema:
            $ref: '#/definitions/FailedResponseDto'
      deprecated: false
  '/account/{accountId}':
    get:
      tags:
        - accounts-controller
      summary: Get account
      produces:
        - application/json
      parameters:
        - name: accountId
          in: path
          description: accountId
          required: true
          type: string
      responses:
        '200':
          description: Account successfully returned
          schema:
            $ref: '#/definitions/AccountResponseDto'
        '400':
          description: Invalid request
          schema:
            $ref: '#/definitions/FailedResponseDto'
        '404':
          description: Account not found
          schema:
            $ref: '#/definitions/FailedResponseDto'
        '500':
          description: Internal server error
          schema:
            $ref: '#/definitions/FailedResponseDto'
      deprecated: false
    delete:
      tags:
        - accounts-controller
      summary: Close account
      produces:
        - application/json
      parameters:
        - name: accountId
          in: path
          description: accountId
          required: true
          type: string
      responses:
        '204':
          description: Account successfully closed
        '400':
          description: Invalid request
          schema:
            $ref: '#/definitions/FailedResponseDto'
        '404':
          description: Account not found
          schema:
            $ref: '#/definitions/FailedResponseDto'
        '500':
          description: Internal server error
          schema:
            $ref: '#/definitions/FailedResponseDto'
      deprecated: false
  '/account/{accountId}/topup':
    post:
      tags:
        - accounts-controller
      summary: Create top up transaction
      consumes:
        - application/json
      produces:
        - application/json
      parameters:
        - name: accountId
          in: path
          description: accountId
          required: true
          type: string
        - in: body
          name: request
          description: request
          required: true
          schema:
            $ref: '#/definitions/TopUpRequestDto'
      responses:
        '202':
          description: Transaction successfully created
          schema:
            $ref: '#/definitions/CreatedTransactionResponseDto'
        '400':
          description: Invalid request
          schema:
            $ref: '#/definitions/FailedResponseDto'
        '500':
          description: Internal server error
          schema:
            $ref: '#/definitions/FailedResponseDto'
      deprecated: false
  '/account/{accountId}/transfer':
    post:
      tags:
        - accounts-controller
      summary: Create transfer transaction
      consumes:
        - application/json
      produces:
        - application/json
      parameters:
        - name: accountId
          in: path
          description: accountId
          required: true
          type: string
        - in: body
          name: request
          description: request
          required: true
          schema:
            $ref: '#/definitions/TransferRequestDto'
      responses:
        '202':
          description: Transaction successfully created
          schema:
            $ref: '#/definitions/CreatedTransactionResponseDto'
        '400':
          description: Invalid request
          schema:
            $ref: '#/definitions/FailedResponseDto'
        '500':
          description: Internal server error
          schema:
            $ref: '#/definitions/FailedResponseDto'
      deprecated: false
  '/account/{accountId}/withdraw':
    post:
      tags:
        - accounts-controller
      summary: Create withdrawal transaction
      consumes:
        - application/json
      produces:
        - application/json
      parameters:
        - name: accountId
          in: path
          description: accountId
          required: true
          type: string
        - name: request
          in: body
          description: request
          required: true
          schema:
            $ref: '#/definitions/WithdrawRequestDto'
      responses:
        '202':
          description: Transaction successfully created
          schema:
            $ref: '#/definitions/CreatedTransactionResponseDto'
        '400':
          description: Invalid request
          schema:
            $ref: '#/definitions/FailedResponseDto'
        '500':
          description: Internal server error
          schema:
            $ref: '#/definitions/FailedResponseDto'
      deprecated: false
definitions:
  AccountResponseDto:
    type: object
    properties:
      balance:
        type: number
        example: 1.01
      id:
        type: string
        example: 74c549b3-49b3-50b3-753e-k1c464f47f1f
      transactions:
        type: array
        items:
          $ref: '#/definitions/TransactionResponseDto'
    title: AccountResponseDto
  CreatedTransactionResponseDto:
    type: object
    properties:
      transactionId:
        type: string
        example: 1dc53df5-703e-49b3-9670-b1c468f47f1f
    title: CreatedTransactionResponseDto
  FailedResponseDto:
    type: object
    properties:
      errorReason:
        type: string
        example: error_reason_example
    title: FailedResponseDto
  OpenAccountRequestDto:
    type: object
    properties:
      email:
        type: string
        example: mantasneviera@gmail.com
    title: OpenAccountRequestDto
  TopUpRequestDto:
    type: object
    properties:
      amount:
        type: number
        example: 1.01
    title: TopUpRequestDto
  TransactionResponseDto:
    type: object
    properties:
      amount:
        type: number
        example: 1.01
      createdAt:
        type: string
        format: date-time
        example: '2020-02-13T18:00:00.000Z'
      errorReason:
        type: string
        example: null
      id:
        type: string
        example: 1dc53df5-703e-49b3-9670-b1c468f47f1f
      issuerAccountId:
        type: string
        example: 74c549b3-49b3-50b3-753e-k1c464f47f1f
      type:
        type: string
        example: CREDIT
    title: TransactionResponseDto
  TransferRequestDto:
    type: object
    properties:
      amount:
        type: number
        example: 1.01
      toAccountId:
        type: string
        example: 74c549b3-49b3-50b3-753e-k1c464f47f1f
    title: TransferRequestDto
  WithdrawRequestDto:
    type: object
    properties:
      amount:
        type: number
        example: 1.01
    title: WithdrawRequestDto
