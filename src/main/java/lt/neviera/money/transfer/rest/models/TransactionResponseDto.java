package lt.neviera.money.transfer.rest.models;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.math.BigDecimal;
import java.time.Instant;

public class TransactionResponseDto {

    public final String id;
    public final Instant createdAt;
    public final String issuerAccountId;
    public final String type;
    public final BigDecimal amount;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public final String errorReason;

    public TransactionResponseDto(String id, Instant createdAt, String issuerAccountId,
                                  String type, BigDecimal amount, String errorReason) {
        this.id = id;
        this.createdAt = createdAt;
        this.issuerAccountId = issuerAccountId;
        this.type = type;
        this.amount = amount;
        this.errorReason = errorReason;
    }
}
