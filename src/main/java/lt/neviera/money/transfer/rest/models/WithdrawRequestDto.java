package lt.neviera.money.transfer.rest.models;

import com.fasterxml.jackson.annotation.JsonCreator;

import java.math.BigDecimal;

public class WithdrawRequestDto {

    public final BigDecimal amount;

    @JsonCreator
    public WithdrawRequestDto(BigDecimal amount) {
        this.amount = amount;
    }
}
