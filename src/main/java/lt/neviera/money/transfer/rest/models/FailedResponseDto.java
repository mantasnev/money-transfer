package lt.neviera.money.transfer.rest.models;

public class FailedResponseDto {

    public final String errorReason;

    public FailedResponseDto(String errorReason) {
        this.errorReason = errorReason;
    }
}
