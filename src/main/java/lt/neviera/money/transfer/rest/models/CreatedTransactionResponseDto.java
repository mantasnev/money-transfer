package lt.neviera.money.transfer.rest.models;

import com.fasterxml.jackson.annotation.JsonCreator;

public class CreatedTransactionResponseDto {

    public final String transactionId;

    @JsonCreator
    public CreatedTransactionResponseDto(String transactionId) {
        this.transactionId = transactionId;
    }
}
