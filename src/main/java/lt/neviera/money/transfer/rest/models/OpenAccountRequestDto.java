package lt.neviera.money.transfer.rest.models;

import com.fasterxml.jackson.annotation.JsonCreator;

public class OpenAccountRequestDto {

    public final String email;

    @JsonCreator
    public OpenAccountRequestDto(String email) {
        this.email = email;
    }
}
