package lt.neviera.money.transfer.rest;

import io.netty.handler.codec.http.HttpResponseStatus;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.ext.web.*;
import io.vertx.ext.web.handler.*;
import lt.neviera.money.transfer.domain.models.Account;
import lt.neviera.money.transfer.events.EventBus;
import lt.neviera.money.transfer.events.models.commands.*;
import lt.neviera.money.transfer.repositories.AccountsRepository;
import lt.neviera.money.transfer.rest.models.*;
import lt.neviera.money.transfer.utils.Json;
import org.slf4j.*;

import java.io.*;
import java.math.BigDecimal;
import java.util.*;

import static io.netty.handler.codec.http.HttpResponseStatus.*;
import static java.util.stream.Collectors.toList;

public final class HttpRouter {

    private static final Logger log = LoggerFactory.getLogger(HttpRouter.class);

    private static final String APPLICATION_JSON = "application/json";

    private static final String ACCOUNT_ID_PARAMETER = "accountId";

    private HttpRouter() {
    }

    public static void init(Router router, EventBus eventBus, AccountsRepository accountsRepository) {

        router.route().handler(BodyHandler.create());
        router.route("/*").handler(StaticHandler.create());

        router.get("/account")
                .produces(APPLICATION_JSON)
                .handler(rc -> handleGetAccounts(accountsRepository, rc));

        router.get("/account/:" + ACCOUNT_ID_PARAMETER)
                .produces(APPLICATION_JSON)
                .handler(rc -> handleGetAccount(accountsRepository, rc));

        router.post("/account")
                .consumes(APPLICATION_JSON)
                .produces(APPLICATION_JSON)
                .handler(rc -> handleCreateAccount(accountsRepository, rc));

        router.delete("/account/:" + ACCOUNT_ID_PARAMETER)
                .produces(APPLICATION_JSON)
                .handler(rc -> handleCloseAccount(accountsRepository, rc));

        router.post("/account/:" + ACCOUNT_ID_PARAMETER + "/topup")
                .produces(APPLICATION_JSON)
                .handler(rc -> handleCreateTopUp(eventBus, accountsRepository, rc));

        router.post("/account/:" + ACCOUNT_ID_PARAMETER + "/withdraw")
                .produces(APPLICATION_JSON)
                .handler(rc -> handleCreateWithdrawal(eventBus, accountsRepository, rc));

        router.post("/account/:" + ACCOUNT_ID_PARAMETER + "/transfer")
                .produces(APPLICATION_JSON)
                .handler(rc -> handleCreateTransfer(eventBus, accountsRepository, rc));
    }

    private static void handleCreateTransfer(EventBus eventBus, AccountsRepository accountsRepository, RoutingContext rc) {
        Optional.ofNullable(rc.request().getParam(ACCOUNT_ID_PARAMETER)).ifPresentOrElse(
                accId -> accountsRepository.find(accId).ifPresentOrElse(
                        a -> tryDeserializeRequest(rc.getBodyAsString(), TransferRequestDto.class).ifPresentOrElse(
                                r -> {
                                    if (r.amount.compareTo(BigDecimal.ZERO) > 0) {
                                        if (accountsRepository.find(r.toAccountId).isPresent() && !accId.equals(r.toAccountId)) {
                                            try {
                                                var debitCommand = new DebitAccount(UUID.randomUUID().toString(), accId, r.amount, r.toAccountId);
                                                eventBus.publish(debitCommand);
                                                respondWithBody(rc, new CreatedTransactionResponseDto(debitCommand.id), ACCEPTED);
                                            } catch (Exception e) {
                                                log.error("Error occurred!", e);
                                                respondWithInternalServerError(rc, "can_not_create_transfer");
                                            }
                                        } else {
                                            respondWithBadRequest(rc, "invalid_receiver_account_id");
                                        }
                                    } else {
                                        respondWithBadRequest(rc, "invalid_amount");
                                    }
                                    },
                                () -> respondWithBadRequest(rc, "can_not_parse_request")),
                        () -> respondWithBadRequest(rc, "invalid_account_id")),
                () -> respondWithBadRequest(rc, "missing_account_id"));
    }

    private static void handleCreateWithdrawal(EventBus eventBus, AccountsRepository accountsRepository, RoutingContext rc) {
        Optional.ofNullable(rc.request().getParam(ACCOUNT_ID_PARAMETER)).ifPresentOrElse(
                accId -> accountsRepository.find(accId).ifPresentOrElse(
                        a -> tryDeserializeRequest(rc.getBodyAsString(), WithdrawRequestDto.class).ifPresentOrElse(
                                r -> {
                                    if (r.amount.compareTo(BigDecimal.ZERO) > 0) {
                                        try {
                                            var debitCommand = new DebitAccount(UUID.randomUUID().toString(), accId, r.amount);
                                            eventBus.publish(debitCommand);
                                            respondWithBody(rc, new CreatedTransactionResponseDto(debitCommand.id), ACCEPTED);
                                        } catch (Exception e) {
                                            log.error("Error occurred!", e);
                                            respondWithInternalServerError(rc, "can_not_create_withdrawal");
                                        }
                                    } else {
                                        respondWithBadRequest(rc, "invalid_amount");
                                    }
                                    },
                                () -> respondWithBadRequest(rc, "can_not_parse_request")),
                        () -> respondWithBadRequest(rc, "invalid_account_id")),
                () -> respondWithBadRequest(rc, "missing_account_id"));
    }

    private static void handleCreateTopUp(EventBus eventBus, AccountsRepository accountsRepository, RoutingContext rc) {
        Optional.ofNullable(rc.request().getParam(ACCOUNT_ID_PARAMETER)).ifPresentOrElse(
                accId -> accountsRepository.find(accId).ifPresentOrElse(
                        a -> tryDeserializeRequest(rc.getBodyAsString(), TopUpRequestDto.class).ifPresentOrElse(
                                r -> {
                                    if (r.amount.compareTo(BigDecimal.ZERO) > 0) {
                                        try {
                                            var creditCommand = new CreditAccount(UUID.randomUUID().toString(), accId, r.amount, accId);
                                            eventBus.publish(creditCommand);
                                            respondWithBody(rc, new CreatedTransactionResponseDto(creditCommand.id), ACCEPTED);
                                        } catch (Exception e) {
                                            log.error("Error occurred!", e);
                                            respondWithInternalServerError(rc, "can_not_create_top_up");
                                        }
                                    } else {
                                        respondWithBadRequest(rc, "invalid_amount");
                                    }
                                    },
                                () -> respondWithBadRequest(rc, "can_not_parse_request")),
                        () -> respondWithBadRequest(rc, "invalid_account_id")),
                () -> respondWithBadRequest(rc, "missing_account_id"));
    }

    private static void handleCloseAccount(AccountsRepository accountsRepository, RoutingContext rc) {
        Optional.ofNullable(rc.request().getParam(ACCOUNT_ID_PARAMETER)).ifPresentOrElse(
                accId -> {
                    try {
                        accountsRepository.find(accId).ifPresentOrElse(
                                acc -> {
                                    try {
                                        accountsRepository.delete(accId);
                                        respondWithNoContent(rc);
                                    } catch (Exception e) {
                                        log.error("Error occurred!", e);
                                        respondWithInternalServerError(rc, "can_not_close_account");
                                    }
                                    },
                                () -> respondWithBody(rc, new FailedResponseDto("account_not_found"), NOT_FOUND));
                    } catch (Exception e) {
                        log.error("Error occurred!", e);
                        respondWithInternalServerError(rc, "can_not_query_accounts");
                    }
                    },
                () -> respondWithBadRequest(rc, "missing_account_id"));
    }

    private static void handleCreateAccount(AccountsRepository accountsRepository, RoutingContext rc) {
        tryDeserializeRequest(rc.getBodyAsString(), OpenAccountRequestDto.class).ifPresentOrElse(
                r -> {
                    try {
                        var account = accountsRepository.save(Account.create(r.email));
                        respondWithBody(rc, AccountResponseDto.from(account), CREATED);
                    } catch (Exception e) {
                        log.error("Error occurred!", e);
                        respondWithInternalServerError(rc, "can_not_persist_account");
                    }
                    },
                () -> respondWithBadRequest(rc, "can_not_parse_request"));
    }

    private static void handleGetAccount(AccountsRepository accountsRepository, RoutingContext rc) {
        Optional.ofNullable(rc.request().getParam(ACCOUNT_ID_PARAMETER)).ifPresentOrElse(
                accId -> {
                    try {
                        accountsRepository.find(accId).ifPresentOrElse(
                                acc -> respondWithBody(rc, AccountResponseDto.from(acc), OK),
                                () -> respondWithBody(rc, new FailedResponseDto("account_not_found"), NOT_FOUND));
                    } catch (Exception e) {
                        log.error("Error occurred!", e);
                        respondWithInternalServerError(rc, "can_not_query_accounts");
                    }
                    },
                () -> respondWithBadRequest(rc, "missing_account_id"));
    }

    private static void handleGetAccounts(AccountsRepository accountsRepository, RoutingContext rc) {
        try {
            var accounts = accountsRepository.findAll().stream()
                    .map(AccountResponseDto::from)
                    .collect(toList());
            respondWithBody(rc, accounts, OK);
        } catch (Exception e) {
            log.error("Error occurred!", e);
            respondWithInternalServerError(rc, "can_not_query_accounts");
        }
    }

    private static <T> Optional<T> tryDeserializeRequest(String jsonString, Class<T> tClass) {
        try {
            return Optional.of(Json.deserialize(jsonString, tClass));
        } catch (UncheckedIOException e) {
            return Optional.empty();
        }
    }

    private static HttpServerResponse jsonResponse(RoutingContext routingContext, HttpResponseStatus responseStatus) {
        return routingContext.response()
                .putHeader("Content-Type", APPLICATION_JSON)
                .setStatusCode(responseStatus.code());
    }

    private static void respondWithBadRequest(RoutingContext routingContext, String errorReason) {
        jsonResponse(routingContext, BAD_REQUEST).end(Json.toJsonString(new FailedResponseDto(errorReason)));
    }

    private static void respondWithInternalServerError(RoutingContext routingContext, String errorReason) {
        jsonResponse(routingContext, INTERNAL_SERVER_ERROR).end(Json.toJsonString(new FailedResponseDto(errorReason)));
    }

    private static <T> void respondWithBody(RoutingContext routingContext, T body, HttpResponseStatus responseStatus) {
        jsonResponse(routingContext, responseStatus).end(Json.toJsonString(body));
    }

    private static void respondWithNoContent(RoutingContext routingContext) {
        jsonResponse(routingContext, NO_CONTENT).end();
    }
}
