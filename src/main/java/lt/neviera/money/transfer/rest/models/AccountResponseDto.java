package lt.neviera.money.transfer.rest.models;

import lt.neviera.money.transfer.domain.models.Account;
import lt.neviera.money.transfer.domain.models.transaction.*;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

import static io.vavr.API.*;
import static io.vavr.Predicates.*;

public class AccountResponseDto {

    public final String id;
    public final String email;
    public final BigDecimal balance;

    public final List<TransactionResponseDto> transactions;

    public AccountResponseDto(String id, String email, BigDecimal balance, List<TransactionResponseDto> transactions) {
        this.id = id;
        this.email = email;
        this.balance = balance;
        this.transactions = transactions;
    }

    public static AccountResponseDto from(Account account) {
        var transactions = account.allTransactions()
                .stream()
                .map(t -> new TransactionResponseDto(
                        t.id, t.createdAt, t.issuerAccountId,
                        t.type.name(), t.amount,
                        Match(t.status).option(Case($(instanceOf(FailedTransaction.class)), ft -> ft.reason)).getOrElse(() -> null)))
                .sorted(Comparator.comparing(t -> t.createdAt))
                .collect(Collectors.toList());

        return new AccountResponseDto(account.id, account.email, account.balance, transactions);
    }
}
