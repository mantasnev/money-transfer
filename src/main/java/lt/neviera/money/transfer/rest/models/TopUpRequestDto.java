package lt.neviera.money.transfer.rest.models;

import com.fasterxml.jackson.annotation.JsonCreator;

import java.math.BigDecimal;

public class TopUpRequestDto {

    public final BigDecimal amount;

    @JsonCreator
    public TopUpRequestDto(BigDecimal amount) {
        this.amount = amount;
    }
}
