package lt.neviera.money.transfer.rest.models;

import com.fasterxml.jackson.annotation.JsonCreator;

import java.math.BigDecimal;

public class TransferRequestDto {

    public final BigDecimal amount;
    public final String toAccountId;

    @JsonCreator
    public TransferRequestDto(BigDecimal amount, String toAccountId) {
        this.amount = amount;
        this.toAccountId = toAccountId;
    }
}
