package lt.neviera.money.transfer.repositories;

import lt.neviera.money.transfer.domain.models.Account;

import java.util.*;

public interface AccountsRepository {
    Account save(Account account);
    Optional<Account> find(String id);
    List<Account> findAll();
    void delete(String id);
}
