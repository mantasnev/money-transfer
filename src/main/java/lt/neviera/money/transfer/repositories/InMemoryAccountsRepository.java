package lt.neviera.money.transfer.repositories;

import lt.neviera.money.transfer.domain.models.Account;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class InMemoryAccountsRepository implements AccountsRepository {

    private final Map<String, Account> storage = new ConcurrentHashMap<>();

    @Override
    public Account save(Account account) {
        storage.put(account.id, account);
        return account;
    }

    @Override
    public Optional<Account> find(String id) {
        return Optional.ofNullable(storage.get(id));
    }

    @Override
    public List<Account> findAll() {
        return List.copyOf(storage.values());
    }

    @Override
    public void delete(String id) {
        storage.remove(id);
    }
}
