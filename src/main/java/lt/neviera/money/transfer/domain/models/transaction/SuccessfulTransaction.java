package lt.neviera.money.transfer.domain.models.transaction;

public final class SuccessfulTransaction implements TransactionStatus {

    public static final SuccessfulTransaction INSTANCE = new SuccessfulTransaction();

    private SuccessfulTransaction() {
    }
}
