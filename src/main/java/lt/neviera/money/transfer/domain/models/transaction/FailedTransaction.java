package lt.neviera.money.transfer.domain.models.transaction;

public final class FailedTransaction implements TransactionStatus {

    public static final FailedTransaction INSUFFICIENT_FUNDS = new FailedTransaction("insufficient_funds");
    public static final FailedTransaction INVALID_AMOUNT = new FailedTransaction("invalid_amount");

    public final String reason;

    private FailedTransaction(String reason) {
        this.reason = reason;
    }

    @Override
    public String toString() {
        return "FailedTransaction{" +
                "reason='" + reason + '\'' +
                '}';
    }
}
