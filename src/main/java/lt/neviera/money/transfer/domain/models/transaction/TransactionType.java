package lt.neviera.money.transfer.domain.models.transaction;

public enum TransactionType {
    CREDIT, DEBIT;
}
