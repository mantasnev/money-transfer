package lt.neviera.money.transfer.domain.models;

import lt.neviera.money.transfer.domain.models.transaction.*;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.*;

import static io.vavr.API.*;
import static io.vavr.Predicates.instanceOf;

public class Account {

    public final String id;
    public final String email;
    public final BigDecimal balance;

    private final Map<String, Transaction> transactions;

    private Account(String id, String email, BigDecimal balance, Map<String, Transaction> transactions) {
        this.id = id;
        this.balance = balance;
        this.email = email;
        this.transactions = transactions;
    }

    public static Account create(String email) {
        return new Account(UUID.randomUUID().toString(), email, BigDecimal.ZERO, Map.of());
    }

    public Account credit(String transactionId, BigDecimal amount) {
        return credit(transactionId, amount, id);
    }

    public Account credit(String transactionId, BigDecimal amount, String issuerAccountId) {
        return findTransactionBy(transactionId)
                .filter(t -> t.type == TransactionType.CREDIT)
                .map(t -> this)
                .orElseGet(() -> applyTransaction(new Transaction(transactionId, Instant.now(), TransactionType.CREDIT,
                        issuerAccountId, amount, validateCreditStatus(amount))));
    }

    public Account debit(String transactionId, BigDecimal amount) {
        return findTransactionBy(transactionId)
                .filter(t -> t.type == TransactionType.DEBIT)
                .map(t -> this)
                .orElseGet(() -> applyTransaction(new Transaction(transactionId, Instant.now(), TransactionType.DEBIT,
                        id, amount, validateDebitStatus(amount))));
    }

    public Optional<Transaction> findTransactionBy(String id) {
        return Optional.ofNullable(transactions.get(id));
    }

    public List<Transaction> allTransactions() {
        return List.copyOf(transactions.values());
    }

    private Account applyTransaction(Transaction transaction) {
        var updatedBalance = Match(transaction.status).of(
                Case($(instanceOf(SuccessfulTransaction.class)), st -> Match(transaction.type).of(
                        Case($(TransactionType.CREDIT), c -> balance.add(transaction.amount)),
                        Case($(TransactionType.DEBIT), c -> balance.subtract(transaction.amount)))),
                Case($(instanceOf(FailedTransaction.class)), ft -> balance));

        var currentTransactions = new HashMap<>(transactions);
        currentTransactions.put(transaction.id, transaction);

        return new Account(id, email, updatedBalance, Map.copyOf(currentTransactions));
    }

    private TransactionStatus validateCreditStatus(BigDecimal amount) {
        if (!isValidAmount(amount))
            return FailedTransaction.INVALID_AMOUNT;

        return SuccessfulTransaction.INSTANCE;
    }

    private TransactionStatus validateDebitStatus(BigDecimal amount) {
        if (!isValidAmount(amount))
            return FailedTransaction.INVALID_AMOUNT;

        if (!isEnoughFunds(amount))
            return FailedTransaction.INSUFFICIENT_FUNDS;

        return SuccessfulTransaction.INSTANCE;
    }

    private boolean isValidAmount(BigDecimal amount) {
        return amount.compareTo(BigDecimal.ZERO) > 0;
    }

    private boolean isEnoughFunds(BigDecimal amount) {
        return balance.subtract(amount).compareTo(BigDecimal.ZERO) >= 0;
    }

    @Override
    public String toString() {
        return "Account{" +
                "id='" + id + '\'' +
                ", email='" + email + '\'' +
                ", balance=" + balance +
                ", transactions=" + transactions +
                '}';
    }
}
