package lt.neviera.money.transfer.domain.models.transaction;

import java.math.BigDecimal;
import java.time.Instant;

public class Transaction {

    public final String id;
    public final Instant createdAt;
    public final TransactionType type;
    public final String issuerAccountId;
    public final BigDecimal amount;
    public final TransactionStatus status;

    public Transaction(String id, Instant createdAt, TransactionType type,
                       String issuerAccountId, BigDecimal amount, TransactionStatus status) {
        this.id = id;
        this.createdAt = createdAt;
        this.type = type;
        this.issuerAccountId = issuerAccountId;
        this.amount = amount;
        this.status = status;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "id='" + id + '\'' +
                ", createdAt=" + createdAt +
                ", type=" + type +
                ", issuerAccountId='" + issuerAccountId + '\'' +
                ", amount=" + amount +
                ", status=" + status +
                '}';
    }
}
