package lt.neviera.money.transfer.events.models.events;

import lt.neviera.money.transfer.events.models.commands.*;

import java.math.BigDecimal;
import java.util.Optional;

public class AccountDebitedSuccessfully extends TransactionResult {

    public final String accountId;
    public final String transactionId;
    public final BigDecimal amount;
    public final Optional<String> creditAccountId;

    public AccountDebitedSuccessfully(String accountId, String transactionId, BigDecimal amount) {
        this.accountId = accountId;
        this.transactionId = transactionId;
        this.amount = amount;
        this.creditAccountId = Optional.empty();
    }

    public AccountDebitedSuccessfully(String accountId, String transactionId, BigDecimal amount, String creditAccountId) {
        this.accountId = accountId;
        this.transactionId = transactionId;
        this.amount = amount;
        this.creditAccountId = Optional.of(creditAccountId);
    }

    public static AccountDebitedSuccessfully from(DebitAccount debitAccount) {
        return debitAccount.creditAccountId
                .map(cai -> new AccountDebitedSuccessfully(debitAccount.accountId, debitAccount.id, debitAccount.amount, cai))
                .orElseGet(() -> new AccountDebitedSuccessfully(debitAccount.accountId, debitAccount.id, debitAccount.amount));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        var that = (AccountDebitedSuccessfully) o;
        if (!accountId.equals(that.accountId)) return false;
        if (!transactionId.equals(that.transactionId)) return false;
        if (!amount.equals(that.amount)) return false;
        return creditAccountId.equals(that.creditAccountId);
    }

    @Override
    public int hashCode() {
        int result = accountId.hashCode();
        result = 31 * result + transactionId.hashCode();
        result = 31 * result + amount.hashCode();
        result = 31 * result + creditAccountId.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "AccountDebitedSuccessfully{" +
                "accountId='" + accountId + '\'' +
                ", transactionId='" + transactionId + '\'' +
                ", amount=" + amount +
                ", creditAccountId=" + creditAccountId +
                '}';
    }
}
