package lt.neviera.money.transfer.events.models.commands;

import lt.neviera.money.transfer.events.models.Command;

import java.math.BigDecimal;

public class ExecuteTransaction implements Command {

    public final String id;
    public final String accountId;
    public final BigDecimal amount;

    public ExecuteTransaction(String id, String accountId, BigDecimal amount) {
        this.id = id;
        this.accountId = accountId;
        this.amount = amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        var that = (ExecuteTransaction) o;
        if (!id.equals(that.id)) return false;
        if (!accountId.equals(that.accountId)) return false;
        return amount.equals(that.amount);
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + accountId.hashCode();
        result = 31 * result + amount.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "ExecuteTransaction{" +
                "id='" + id + '\'' +
                ", accountId='" + accountId + '\'' +
                ", amount=" + amount +
                '}';
    }
}
