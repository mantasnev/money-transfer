package lt.neviera.money.transfer.events.models.events;

import lt.neviera.money.transfer.events.models.commands.ExecuteTransaction;

import java.util.Objects;

public class AccountDoesNotExists extends TransactionResult {

    public final ExecuteTransaction executedTransaction;

    public AccountDoesNotExists(ExecuteTransaction executedTransaction) {
        this.executedTransaction = executedTransaction;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        var that = (AccountDoesNotExists) o;
        return Objects.equals(executedTransaction, that.executedTransaction);
    }

    @Override
    public int hashCode() {
        return executedTransaction != null ? executedTransaction.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "AccountDoesNotExists{" +
                "executedTransaction=" + executedTransaction +
                '}';
    }
}
