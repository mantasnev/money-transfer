package lt.neviera.money.transfer.events;

import io.reactivex.Flowable;
import io.reactivex.processors.*;
import lt.neviera.money.transfer.events.models.*;

public class EventBus {

    private final FlowableProcessor<Event> eventsProcessor = PublishProcessor.<Event>create().toSerialized();
    private final FlowableProcessor<Command> commandsProcessor = PublishProcessor.<Command>create().toSerialized();

    public <E extends Event> void publish(E event) {
        eventsProcessor.onNext(event);
    }

    public <E extends Event> Flowable<E> observeEvents(Class<E> eventType) {
        return eventsProcessor.ofType(eventType);
    }

    public <C extends Command> void publish(C command) {
        commandsProcessor.onNext(command);
    }

    public <C extends Command> Flowable<C> observeCommands(Class<C> commandType) {
        return commandsProcessor.ofType(commandType);
    }

    public void shutdown(){
        eventsProcessor.onComplete();
        commandsProcessor.onComplete();
    }
}
