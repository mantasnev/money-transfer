package lt.neviera.money.transfer.events.processors;

import io.reactivex.Flowable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import lt.neviera.money.transfer.domain.models.Account;
import lt.neviera.money.transfer.domain.models.transaction.*;
import lt.neviera.money.transfer.events.EventBus;
import lt.neviera.money.transfer.events.models.Event;
import lt.neviera.money.transfer.events.models.commands.*;
import lt.neviera.money.transfer.events.models.events.*;
import lt.neviera.money.transfer.repositories.AccountsRepository;
import org.slf4j.*;

import java.util.function.*;

import static io.vavr.API.*;
import static io.vavr.Predicates.instanceOf;

public class TransactionsProcessor {

    private static final Logger log = LoggerFactory.getLogger(TransactionsProcessor.class);

    private final AccountsRepository accountsRepository;

    public TransactionsProcessor(AccountsRepository accountsRepository) {
        this.accountsRepository = accountsRepository;
    }

    public Disposable subscribe(EventBus eventBus) {
        return eventBus
                .observeCommands(ExecuteTransaction.class)
                .onBackpressureBuffer()
                .observeOn(Schedulers.single())
                .doOnNext(t -> log.info("Command received: {}", t))
                .concatMap(t -> Match(t).of(
                        Case($(instanceOf(CreditAccount.class)), ca -> handle(t,
                                acc -> acc.credit(t.id, t.amount, ca.issuerAccountId),
                                () -> AccountCreditedSuccessfully.from(ca))),
                        Case($(instanceOf(DebitAccount.class)), da -> handle(t,
                                acc -> acc.debit(t.id, t.amount),
                                () -> AccountDebitedSuccessfully.from(da)))
                ))
                .doOnNext(eventBus::publish)
                .retry()
                .subscribe(
                        e -> log.info("Event published: {}", e),
                        err -> log.error("Error occurred!", err),
                        () -> log.info("Transactions processor subscription completed.")
                );
    }

    private Flowable<Event> handle(ExecuteTransaction transaction, UnaryOperator<Account> onExecute, Supplier<Event> onSuccess) {
        return Flowable.just(accountsRepository.find(transaction.accountId)
                .map(onExecute)
                .map(accountsRepository::save)
                .flatMap(acc -> acc.findTransactionBy(transaction.id))
                .map(tr -> Match(tr.status).of(
                        Case($(instanceOf(SuccessfulTransaction.class)), onSuccess),
                        Case($(instanceOf(FailedTransaction.class)),
                                ft -> new TransactionFailed(transaction.id, transaction.accountId, ft.reason))
                ))
                .orElseGet(() -> new AccountDoesNotExists(transaction)));
    }
}
