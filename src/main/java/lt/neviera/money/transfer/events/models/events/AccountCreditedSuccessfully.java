package lt.neviera.money.transfer.events.models.events;

import lt.neviera.money.transfer.events.models.commands.CreditAccount;

import java.math.BigDecimal;
import java.util.Objects;

public class AccountCreditedSuccessfully extends TransactionResult {

    public final String accountId;
    public final String transactionId;
    public final BigDecimal amount;
    public final String issuerAccountId;

    public AccountCreditedSuccessfully(String accountId, String transactionId, BigDecimal amount, String issuerAccountId) {
        this.accountId = accountId;
        this.transactionId = transactionId;
        this.amount = amount;
        this.issuerAccountId = issuerAccountId;
    }

    public static AccountCreditedSuccessfully from(CreditAccount creditAccount) {
        return new AccountCreditedSuccessfully(creditAccount.accountId, creditAccount.id, creditAccount.amount, creditAccount.issuerAccountId);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        var that = (AccountCreditedSuccessfully) o;
        if (!Objects.equals(accountId, that.accountId)) return false;
        if (!Objects.equals(transactionId, that.transactionId)) return false;
        if (!Objects.equals(amount, that.amount)) return false;
        return Objects.equals(issuerAccountId, that.issuerAccountId);
    }

    @Override
    public int hashCode() {
        int result = accountId != null ? accountId.hashCode() : 0;
        result = 31 * result + (transactionId != null ? transactionId.hashCode() : 0);
        result = 31 * result + (amount != null ? amount.hashCode() : 0);
        result = 31 * result + (issuerAccountId != null ? issuerAccountId.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "AccountCreditedSuccessfully{" +
                "accountId='" + accountId + '\'' +
                ", transactionId='" + transactionId + '\'' +
                ", amount=" + amount +
                ", issuerAccountId='" + issuerAccountId + '\'' +
                '}';
    }
}
