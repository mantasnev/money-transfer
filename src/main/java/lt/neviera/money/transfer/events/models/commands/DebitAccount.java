package lt.neviera.money.transfer.events.models.commands;

import java.math.BigDecimal;
import java.util.Optional;

public class DebitAccount extends ExecuteTransaction {

    public final Optional<String> creditAccountId;

    public DebitAccount(String transactionId, String accountId, BigDecimal amount) {
        super(transactionId, accountId, amount);
        this.creditAccountId = Optional.empty();
    }

    public DebitAccount(String transactionId, String accountId, BigDecimal amount, String creditAccountId) {
        super(transactionId, accountId, amount);
        this.creditAccountId = Optional.of(creditAccountId);
    }

    @Override
    public String toString() {
        return "DebitAccount{" +
                "id='" + id + '\'' +
                ", accountId='" + accountId + '\'' +
                ", amount=" + amount +
                ", creditAccountId=" + creditAccountId +
                '}';
    }
}
