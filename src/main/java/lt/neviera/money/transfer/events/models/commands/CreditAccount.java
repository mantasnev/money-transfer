package lt.neviera.money.transfer.events.models.commands;

import java.math.BigDecimal;

public class CreditAccount extends ExecuteTransaction {

    public final String issuerAccountId;

    public CreditAccount(String transactionId, String accountId, BigDecimal amount, String issuerAccountId) {
        super(transactionId, accountId, amount);
        this.issuerAccountId = issuerAccountId;
    }

    public boolean isTopUp() {
        return accountId.equals(issuerAccountId);
    }

    public CreditAccount refundTransaction() {
        return new CreditAccount(id + "_REFUND", issuerAccountId, amount, accountId);
    }

    @Override
    public String toString() {
        return "CreditAccount{" +
                "id='" + id + '\'' +
                ", accountId='" + accountId + '\'' +
                ", amount=" + amount +
                ", issuerAccountId=" + issuerAccountId +
                '}';
    }
}
