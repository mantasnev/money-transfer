package lt.neviera.money.transfer.events.models.events;

public class TransactionFailed extends TransactionResult {

    public final String id;
    public final String accountId;
    public final String reason;

    public TransactionFailed(String id, String accountId, String reason) {
        this.id = id;
        this.accountId = accountId;
        this.reason = reason;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        var that = (TransactionFailed) o;
        if (!id.equals(that.id)) return false;
        if (!accountId.equals(that.accountId)) return false;
        return reason.equals(that.reason);
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + accountId.hashCode();
        result = 31 * result + reason.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "TransactionFailed{" +
                "id='" + id + '\'' +
                ", accountId='" + accountId + '\'' +
                ", reason='" + reason + '\'' +
                '}';
    }
}
