package lt.neviera.money.transfer.events.processors;

import io.reactivex.Flowable;
import io.reactivex.disposables.Disposable;
import io.vavr.control.Option;
import lt.neviera.money.transfer.events.EventBus;
import lt.neviera.money.transfer.events.models.commands.CreditAccount;
import lt.neviera.money.transfer.events.models.events.*;
import org.slf4j.*;

import static io.vavr.API.*;
import static io.vavr.Predicates.instanceOf;
import static java.util.function.Predicate.not;

public class TransactionResultsProcessor {

    private static final Logger log = LoggerFactory.getLogger(TransactionResultsProcessor.class);

    public Disposable subscribe(EventBus eventBus) {
        return eventBus.observeEvents(TransactionResult.class)
                .onBackpressureBuffer()
                .doOnNext(tr -> log.info("Event received: {}", tr))
                .concatMap(tr -> Match(tr).option(
                        Case($(instanceOf(AccountDebitedSuccessfully.class)),
                                ds -> Option.ofOptional(ds.creditAccountId)
                                        .map(cas -> new CreditAccount(ds.transactionId, cas, ds.amount, ds.accountId))),
                        Case($(instanceOf(AccountDoesNotExists.class)),
                                ne -> Match(ne.executedTransaction).option(Case($(instanceOf(CreditAccount.class)), ca -> ca))
                                        .filter(not(CreditAccount::isTopUp))
                                        .map(CreditAccount::refundTransaction)))
                        .flatMap(ca -> ca.map(Flowable::just))
                        .getOrElse(Flowable::empty)
                )
                .doOnNext(eventBus::publish)
                .retry()
                .subscribe(
                        e -> log.info("Command published: {}", e),
                        err -> log.error("Error occurred!", err),
                        () -> log.info("Transaction results subscription completed.")
                );
    }
}
