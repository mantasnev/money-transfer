package lt.neviera.money.transfer;

import io.vertx.core.Vertx;
import io.vertx.core.http.*;
import io.vertx.ext.web.Router;
import lt.neviera.money.transfer.domain.models.Account;
import lt.neviera.money.transfer.events.EventBus;
import lt.neviera.money.transfer.events.models.commands.*;
import lt.neviera.money.transfer.events.processors.*;
import lt.neviera.money.transfer.repositories.*;
import lt.neviera.money.transfer.rest.HttpRouter;
import org.slf4j.*;

import java.math.BigDecimal;
import java.util.UUID;

public class Application {

    private static final Logger log = LoggerFactory.getLogger(Application.class);

    private static final int DEFAULT_APP_PORT = 7777;

    public static void main(String[] args) {
        log.info("Starting application...");

        var eventBus = new EventBus();
        var accountsRepository = new InMemoryAccountsRepository();
        var transactionsProcessor = new TransactionsProcessor(accountsRepository);
        var transactionsProcessorSubscription = transactionsProcessor.subscribe(eventBus);
        var transactionResultsProcessor = new TransactionResultsProcessor();
        var transactionResultsProcessorSubscription = transactionResultsProcessor.subscribe(eventBus);

        var vertx = Vertx.vertx();
        var httpServer = initHttpServer(vertx, eventBus, accountsRepository, DEFAULT_APP_PORT);

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            log.info("Shutting down application...");
            httpServer.close();
            vertx.close();
            transactionsProcessorSubscription.dispose();
            transactionResultsProcessorSubscription.dispose();
            eventBus.shutdown();
        }));

        log.info("Application started (port: {})", DEFAULT_APP_PORT);
    }

    public static HttpServer initHttpServer(Vertx vertx, EventBus eventBus, AccountsRepository accountsRepo, int port) {
        var router = Router.router(vertx);
        HttpRouter.init(router, eventBus, accountsRepo);
        var httpServerOptions = new HttpServerOptions().setPort(port).setSsl(false);
        return vertx.createHttpServer(httpServerOptions).requestHandler(router).listen();
    }
}
