# Money Transfer

REST API for money transfer.

# Motivation
Technical task to demonstrate programming skills and experience as a software engineer.

# Decisions & Assumptions
 - It was decided to skip currencies and API authentication/authorization because it is not relevant for this demo application.
 - It was decided to use simple in memory storage for the sake of simplicity, it could be replaced with any storage which implements provided interface in the future.
 - All transactions are idempotent and ensures exactly once semantics.
 - Unit and integration tests are written.
 - Swagger is used as an API documentation. 
 - Lightweight libraries are used for: json parsing (`Jackson`), http server (`Vertx`), enhanced pattern matching (`Vavr`), reactive streams (`RxJava`).
 - Application is very scalable - `Kafka` could be used instead of `Reactive EventBus` to be able to handle thousands of transactions with exactly once semantics.
 - Application is configured to run in `Docker` containers to be able to easily deploy it to `K8s`, `AWS`, ... , or just simply run it locally in any environment.

### Installation & Run

1. Locally (**Java 11** and **Maven** required):

```sh
$ mvn clean package
$ java -jar target/money-transfer-1.0.0-SNAPSHOT.jar
```

2. Docker:
```sh
$ docker build -t money-transfer:latest .
$ docker run -d -p 7777:7777 money-transfer:latest
```

### Links
Swagger documentation available at `http://localhost:7777`

### Usage

The API could be tested using Swagger UI or just simple `curl`.

##### Examples

1. Create some accounts:
```sh
$ curl -X GET "http://localhost:7777/account" -H "accept: application/json"
$ curl -X POST "http://localhost:7777/account" -H "accept: application/json" -H "Content-Type: application/json" -d "{ \"email\": \"firstemail@gmail.com\"}"
$ curl -X POST "http://localhost:7777/account" -H "accept: application/json" -H "Content-Type: application/json" -d "{ \"email\": \"secondemail@gmail.com\"}"
$ curl -X GET "http://localhost:7777/account" -H "accept: application/json"
```
2. Top-up first account:
```sh
$ curl -X POST "http://localhost:7777/account/$FIRST_ACCOUNT_ID/topup" -H "accept: application/json" -H "Content-Type: application/json" -d "{ \"amount\": 10}"
```
3. Transfer some money to second account:
```sh
$ curl -X POST "http://localhost:7777/account/$FIRST_ACCOUNT_ID/transfer" -H "accept: application/json" -H "Content-Type: application/json" -d "{ \"amount\": 3, \"toAccountId\": \"$SECOND_ACCOUNT_ID\"}"
$ curl -X POST "http://localhost:7777/account/$FIRST_ACCOUNT_ID/transfer" -H "accept: application/json" -H "Content-Type: application/json" -d "{ \"amount\": 3, \"toAccountId\": \"$SECOND_ACCOUNT_ID\"}"
```
4. Withdraw money from second account:
```sh
$ curl -X POST "http://localhost:7777/account/$SECOND_ACCOUNT_ID/withdraw" -H "accept: application/json" -H "Content-Type: application/json" -d "{ \"amount\": 5}"
```
5. Check accounts:
```sh
$ curl -X GET "http://localhost:7777/account/$FIRST_ACCOUNT_ID" -H "accept: application/json"
$ curl -X GET "http://localhost:7777/account/$SECOND_ACCOUNT_ID" -H "accept: application/json"
```
6. Delete accounts:
```sh
$ curl -X DELETE "http://localhost:7777/account/$FIRST_ACCOUNT_ID" -H "accept: application/json"
$ curl -X DELETE "http://localhost:7777/account/$SECOND_ACCOUNT_ID" -H "accept: application/json"
```