FROM maven:3-jdk-11 AS builder
COPY . /tmp/build
WORKDIR /tmp/build
RUN mvn -B clean test package

FROM openjdk:11-jre-slim
COPY --from=builder /tmp/build/target/money-transfer-1.0.0-SNAPSHOT.jar /opt/app/money-transfer-1.0.0-SNAPSHOT.jar
CMD [ \
    "java", \
    "-jar", \
    "/opt/app/money-transfer-1.0.0-SNAPSHOT.jar" \
]
EXPOSE 7777